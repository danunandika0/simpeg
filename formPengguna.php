<?php 
    include 'header.php';
    include 'connect.php';

    $act = 'add';
    
    if(!empty($_GET['username'])) {
        $sql = 'SELECT * FROM pengguna WHERE username="'.$_GET['username'].'"';
        
        $query = mysqli_query($conn, $sql); 
   
        if(mysqli_num_rows($query)) {
            $act = 'edit';

            $row = mysqli_fetch_object($query);
        }
    }
?>

<h1 class="mt-3 mb-3 container">Form Pengguna</h1>
<form action="savePengguna.php" method="POST" class="container" >

    <div class="mb-3">
        <label class="form-label">Username</label>
        <input type="text" class="form-control" name="username" placeholder="Username" value="<?php if ($act == 'edit') echo $row->username; ?>"
            required>
    </div>

    <div class="mb-3">
        <label class="form-label">Password</label>
        <input type="text" class="form-control" name="password" placeholder="Password" value="<?php if ($act == 'edit') echo $row->password; ?>"
            required>
    </div>
    
    <div class="mb-3">
        <input type="submit" value="Simpan" class="btn btn-sm btn-success">
        <input type="submit" value="Batal" class="btn btn-sm btn-warning" href="pengguna.php">
    </div>
</form>
